<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\ProductType;
use App\Http\ViewComposers;
use App\Cart;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

   
    public function boot()
    {
        view()->composer(['header','page.dat_hang'], function($view)
        {
            $loai_sp = ProductType::all();

            $view->with('loai_sp', $loai_sp);
        });

        view()->composer(['header','page.dat_hang'], function($view)
        {
             if(Session('cart'))
            {
                $oldCart = Session::get('cart');
                $cart = new Cart($oldCart);
                $view->with(['cart'=>Session::get('cart'), 'product_cart'=>$cart->items, 'totalPrice'=>$cart->totalPrice, 'totalQty'=>$cart->totalQty]);
            }

        });
    }
    
     /**
     * Bootstrap any application services.
     *
     * @return void
     */
}
